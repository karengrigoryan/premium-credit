<?php
/* Template Name: News */
?>
<?php get_header(); ?>

<main id="news" class="page-content">
  <div class="page-header bg-dark">
    <div class="d-flex flex-column align-items-center justify-content-center py-7">
      <h2 class="title text-center text-white mb-0 px-3"><?php the_title() ?></h2>
    </div>
  </div>

  <div class="container mt-5">
    <div class="text-muted small">
			<?php the_field( 'news_short_intro_text' ) ?>
    </div>

    <div class="row latest-news my-3">
			<?php $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
			$args        = array( 'post_type' => 'post', 'cat' => '4', 'posts_per_page' => 10, 'paged' => $paged );
			query_posts( $args ); ?>
			<?php if ( have_posts() ) : ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php $image = get_field( 'homepage_latest_news_credit_cover_image' );
					if ( ! empty( $image ) ):
						$size  = 'medium';
						$thumb = $image['sizes'][ $size ];
						?>
					<?php endif; ?>

          <div class="col-lg-6 my-2">
            <div class="card h-100">
              <a href="<?php the_permalink() ?>">
                <img class="card-img-top" src="<?php echo $thumb; ?>" alt="<?php echo $imageAlt ?>">
              </a>

              <div class="card-body d-flex flex-column">
                <a class="text-dark" href="<?php the_permalink() ?>"><h5 class="mb-0"><?php the_title() ?></h5></a>

                <p class="card-text">
                  <small class="text-muted"><?php echo get_the_date(); ?></small>
                </p>

                <span class="text-muted"><?php the_field( 'homepage_latest_news_credit_short_description' ) ?></span>

                <a href="<?php the_permalink() ?>" class="btn btn-sm btn-warning mr-auto mt-auto">Կարդալ ավելին</a>
              </div>
            </div>
          </div>

				<?php endwhile; ?>

				<?php if ( 'en_US' == get_locale() ): ?>
					<?php the_posts_pagination( array(
							'mid_size'  => 2,
							'prev_text' => __( 'Previous', 'textdomain' ),
							'next_text' => __( 'Next', 'textdomain' ),
					) ); ?>
				<?php endif; ?>

				<?php if ( 'ru_RU' == get_locale() ): ?>
					<?php the_posts_pagination( array(
							'mid_size'  => 2,
							'prev_text' => __( 'Предыдущие', 'textdomain' ),
							'next_text' => __( 'Следующие', 'textdomain' ),
					) ); ?>
				<?php endif; ?>

				<?php if ( 'hy' == get_locale() ): ?>
					<?php the_posts_pagination( array(
							'mid_size'  => 2,
							'prev_text' => __( 'Նախորդ', 'textdomain' ),
							'next_text' => __( 'Հաջորդ', 'textdomain' ),
					) ); ?>
				<?php endif; ?>
			  <?php else : ?>
			<?php endif; ?>
    </div>
  </div>
</main>

<?php get_footer(); ?>
