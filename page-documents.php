<?php /* Template Name: Documents */ ?>
<?php get_header(); ?>

<?php if( 'en_US' == get_locale() ): ?>
  <?php $licenses_title = 'License and registation certificate' ?>
  <?php $statute_title = 'Statute' ?>
<?php endif; ?>

<?php if( 'ru_RU' == get_locale() ): ?>
  <?php $licenses_title = 'Лицензия и акт регистрации' ?>
  <?php $statute_title = 'Регламент' ?>
<?php endif; ?>

<?php if( 'hy' == get_locale() ): ?>
  <?php $licenses_title = 'Լիցենզիա և վկայական' ?>
  <?php $statute_title = 'Կանոնադրություն' ?>
<?php endif; ?>

  <main class="bg-faded pb-8">
    <div class="page-header bg-dark">
      <div class="d-flex flex-column align-items-center justify-content-center py-7">
        <h2 class="title text-center text-white mb-0 px-3"><?php the_title() ?></h2>
      </div>
    </div>

    <div class="container">
      <div class="my-5">
        <?php the_field('page_intro_text') ?>
      </div>

      <div class="bg-white shadow rounded mb-5">
        <?php if( have_rows('page_documents_licenses') ): ?>
          <h4 class="text-center rounded-top mb-0 pt-2"><?php echo $licenses_title ?></h4>

          <hr class="mb-0">

          <div class="py-2 px-3">
            <?php while ( have_rows('page_documents_licenses') ) : the_row(); ?>
              <a href="<?php the_sub_field('license_file') ?>" class="d-block text-muted" target="_blank"><ins><?php the_sub_field('license_name') ?></ins></a>
            <?php endwhile; ?>
          </div>
        <?php endif; ?>
      </div>

      <div class="bg-white shadow rounded">
        <?php if( have_rows('page_documents_statute') ): ?>
          <h4 class="text-center rounded-top mb-0 pt-2"><?php echo $statute_title ?></h4>

          <hr class="mb-0">

          <div class="py-2 px-3">
            <?php while ( have_rows('page_documents_statute') ) : the_row(); ?>
              <a href="<?php the_sub_field('statute_file') ?>" class="d-block text-muted" target="_blank"><ins><?php the_sub_field('statute_name') ?></ins></a>
            <?php endwhile; ?>
          </div>
        <?php endif; ?>
      </div>

    </div>
  </main>
<?php get_footer(); ?>
