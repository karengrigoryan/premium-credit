<?php /* Template Name: Structure */ ?>

<?php get_header(); ?>
  <main class="bg-faded">
    <div class="page-header bg-warning">
      <div class="d-flex flex-column align-items-center justify-content-center py-7">
        <h2 class="title text-center text-white mb-0 px-3"><?php the_title() ?></h2>
      </div>
    </div>

    <div class="container">
      <div class="my-4">
        <?php the_field('page_content') ?>

        <hr class="my-4">

	      <?php if ( 'en_US' == get_locale() ): ?>
          <img src="<?php echo get_template_directory_uri(); ?>/src/img/page-structure/structure-en.svg" alt="Premium Credit Armenia" class="img-fluid">
	      <?php endif; ?>

	      <?php if ( 'ru_RU' == get_locale() ): ?>
          <img src="<?php echo get_template_directory_uri(); ?>/src/img/page-structure/structure-ru.svg" alt="Premium Credit Armenia" class="img-fluid">
	      <?php endif; ?>

	      <?php if ( 'hy' == get_locale() ): ?>
          <img src="<?php echo get_template_directory_uri(); ?>/src/img/page-structure/structure-am.svg" alt="Premium Credit Armenia" class="img-fluid">
        <?php endif; ?>

        <hr class="mt-3 mb-2">

	      <?php if ( 'en_US' == get_locale() ): ?>
		      <p class="text-muted small mb-0">Scan version of UCO's official structure confirmed by the Central Bank of RA, can be download via <a href="<?php the_field('page_structure_original') ?>" target="_blank">this link</a>։</p>
	      <?php endif; ?>

	      <?php if ( 'ru_RU' == get_locale() ): ?>
		      <p class="text-muted small mb-0">* Скан версию оригинального документа структуры УКО подтвержденной ЦБ РА, можете скачать по указанной <a href="<?php the_field('page_structure_original') ?>" target="_blank">ссылке</a>.</p>
	      <?php endif; ?>

	      <?php if ( 'hy' == get_locale() ): ?>
		      <p class="text-muted small mb-0">* ՀՀ ԿԲ-ի կողմից հաստատված կանոնդրության բնօրինակի scan տարբերակը կարող եք ներբեռնել հետևյալ <a href="<?php the_field('page_structure_original') ?>" target="_blank">հղումով</a>։</p>
	      <?php endif; ?>
      </div>
    </div>

  </main>
<?php get_footer(); ?>
