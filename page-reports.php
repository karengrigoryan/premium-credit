<?php /* Template Name: Reports */ ?>
<?php get_header(); ?>

<?php if ( 'en_US' == get_locale() ): ?>
	<?php $reports_year_title = 'Annual Financial Statements' ?>
	<?php $reports_interim_title = 'Interim Financial Statements' ?>
	<?php $reports_economic_title = 'Key Prudential Standards' ?>
<?php endif; ?>

<?php if ( 'ru_RU' == get_locale() ): ?>
	<?php $reports_year_title = 'Ежегодная Финансовая отчетность' ?>
	<?php $reports_interim_title = 'Промежуточная Финансовая отчетность' ?>
	<?php $reports_economic_title = 'Основные экономические нормативы' ?>
<?php endif; ?>

<?php if ( 'hy' == get_locale() ): ?>
	<?php $reports_year_title = 'Տարեկան ֆինանսական հաշվետվություններ' ?>
	<?php $reports_interim_title = 'Միջանկյալ ֆինանսական հաշվետվություններ' ?>
	<?php $reports_economic_title = 'Հիմնական տնտեսական նորմատիվներ' ?>
<?php endif; ?>

<main id="page-reports" class="bg-faded pb-8">
  <div class="page-header bg-dark">
    <div class="d-flex flex-column align-items-center justify-content-center py-7">
      <h2 class="title text-center text-white mb-0 px-3"><?php the_title() ?></h2>
    </div>
  </div>

  <div class="container">
    <div class="my-5">
			<?php the_field( 'page_intro_text' ) ?>
    </div>

    <div id="reports-accordion" role="tablist">
      <div class="card shadow mb-3">
        <div class="card-header bg-primary-gradient py-0" role="tab">
          <h5 class="mb-0">
            <a class="collapsed d-flex text-white py-2" data-toggle="collapse" href="#reports-collapse-1" aria-expanded="true" aria-controls="reports-collapse-1"><?php echo $reports_year_title ?>
              <i class="fa fa-chevron-up ml-auto" aria-hidden="true"></i></a>
          </h5>
        </div>

        <div id="reports-collapse-1" class="collapse" role="tabpanel" data-parent="#reports-accordion">
          <div class="card-body">
            <p class="mb-1 text-center lead">2017</p>
            <hr>
	          <?php while ( have_rows( 'reports_yearly' ) ): the_row();
		          $title = get_sub_field( 'title' );
		          $url   = get_sub_field( 'url' );
		          ?>

		          <?php if ( $url ): ?>
                <a href="<?php echo $url; ?>" target="_blank" class="d-block"><?php echo $title ?></a>
		          <?php endif; ?>
	          <?php endwhile; ?>
          </div>
        </div>
      </div>

			<?php if ( have_rows( 'reports_quarter' ) ): ?>
        <div class="card shadow mb-3">
          <div class="card-header bg-primary-gradient py-0" role="tab">
            <h5 class="mb-0">
              <a class="d-flex text-white py-2" data-toggle="collapse" href="#reports-collapse-2" aria-expanded="true" aria-controls="reports-collapse-2"><?php echo $reports_interim_title ?>
                <i class="fa fa-chevron-up ml-auto" aria-hidden="true"></i></a>
            </h5>
          </div>

          <div id="reports-collapse-2" class="collapse show" role="tabpanel" data-parent="#reports-accordion">
            <div class="card-body">
              <p class="mb-1 text-center lead">2017</p>
              <hr>
							<?php while ( have_rows( 'reports_quarter' ) ): the_row();
								$title = get_sub_field( 'title' );
								$url   = get_sub_field( 'url' );
								?>

								<?php if ( $url ): ?>
                  <a href="<?php echo $url; ?>" target="_blank" class="d-block"><?php echo $title ?></a>
								<?php endif; ?>
							<?php endwhile; ?>

              <div class="w-100 py-2"></div>

              <p class="mb-1 text-center lead">2018</p>
              <hr>
							<?php while ( have_rows( 'reports_quarter_2018' ) ): the_row();
								$title = get_sub_field( 'title' );
								$url   = get_sub_field( 'url' );
								?>

								<?php if ( $url ): ?>
                  <a href="<?php echo $url; ?>" target="_blank" class="d-block"><?php echo $title ?></a>
								<?php endif; ?>
							<?php endwhile; ?>
            </div>
          </div>
        </div>
			<?php endif; ?>

			<?php if ( have_rows( 'reports_econmic' ) ): ?>
        <div class="card shadow mb-3">
          <div class="card-header bg-primary-gradient py-0" role="tab">
            <h5 class="mb-0">
              <a class="collapsed d-flex text-white py-2" data-toggle="collapse" href="#reports-collapse-3" aria-expanded="true" aria-controls="reports-collapse-3"><?php echo $reports_economic_title ?>
                <i class="fa fa-chevron-up ml-auto" aria-hidden="true"></i></a>
            </h5>
          </div>

          <div id="reports-collapse-3" class="collapse" role="tabpanel" data-parent="#reports-accordion">
            <div class="card-body">
              <p class="mb-1 text-center lead">2017</p>
              <hr>
							<?php while ( have_rows( 'reports_econmic' ) ): the_row();
								$title = get_sub_field( 'title' );
								$url   = get_sub_field( 'url' );
								?>

								<?php if ( $url ): ?>
                  <a href="<?php echo $url; ?>" target="_blank" class="d-block"><?php echo $title ?></a>
								<?php endif; ?>
							<?php endwhile; ?>

              <div class="w-100 py-2"></div>

              <p class="mb-1 text-center lead">2018</p>
              <hr>
							<?php while ( have_rows( 'reports_econmic_2018' ) ): the_row();
								$title = get_sub_field( 'title' );
								$url   = get_sub_field( 'url' );
								?>

								<?php if ( $url ): ?>
                  <a href="<?php echo $url; ?>" target="_blank" class="d-block"><?php echo $title ?></a>
								<?php endif; ?>
							<?php endwhile; ?>
            </div>
          </div>
        </div>
			<?php endif; ?>
    </div>
  </div>
</main>
<?php get_footer(); ?>
