<?php /* Template Name: Homepage */ ?>
<?php get_header(); ?>

<!-- variables -->
<?php if ( 'en_US' == get_locale() ): ?>
	<?php $news_page_id = 226;
	$news_page_title    = 'All news';
	$news_read_more     = 'Read more' ?>
<?php endif; ?>

<?php if ( 'ru_RU' == get_locale() ): ?>
	<?php $news_page_id = 225;
	$news_page_title    = 'Все новости';
	$news_read_more     = 'Читать далее' ?>
<?php endif; ?>

<?php if ( 'hy' == get_locale() ): ?>
	<?php $news_page_id = 221;
	$news_page_title    = 'Բոլոր նորությունները';
	$news_read_more     = 'Կարդալ ավելին' ?>
<?php endif; ?>

<!-- page content -->
<main id="homepage" class="page-content">
  <!-- carousel -->
	<?php if ( have_rows( 'homepage_slider' ) ): ?>
    <div id="homepage-carousel" class="owl-carousel owl-theme">
			<?php while ( have_rows( 'homepage_slider' ) ): the_row();
				$image = get_sub_field( 'homepage_slide_image' );
				$title = get_sub_field( 'homepage_slide_title' );
				$text  = get_sub_field( 'homepage_slide_text' );
				$link  = get_sub_field( 'homepage_slide_link' );
				?>
        <div class="item">
					<?php if ( $image ): ?>
            <img src="<?php echo $image ?>" alt="<?php echo $imageAlt ?>">
					<?php endif; ?>

          <div class="slide-content d-flex align-items-center flex-wrap">
            <div class="slide-content-inner my-3">
							<?php if ( $link ): ?>
              <a href="<?php echo $link ?>">
								<?php endif; ?>

								<?php if ( $title ): ?>
                  <h3 class="slide-title mx-4 mx-md-2 mb-0 pb-1 px-lg-1 text-white text-md-center"><?php echo $title ?></h3>
								<?php endif; ?>

								<?php if ( $text ): ?>
                  <p class="slide-text mx-2 px-2 mb-0 small text-lg-justify text-md-center"><?php echo $text ?></p>
								<?php endif; ?>

								<?php if ( $link ): ?>
              </a>
						<?php endif; ?>
            </div>
          </div>
        </div>
			<?php endwhile; ?>
    </div>
	<?php endif; ?>

  <!-- credits carousel -->
  <div class="credits-carousel owl-carousel">
		<?php $homepage_credits_query = new WP_Query( 'posts_per_page=10&cat=3' );
		while ( $homepage_credits_query->have_posts() ) : $homepage_credits_query->the_post(); ?>

			<?php $image = get_field( 'credit_cover_image' );
			if ( ! empty( $image ) ):
				$size  = 'medium';
				$thumb = $image['sizes'][ $size ];
				?>
			<?php endif; ?>

      <a href="<?php the_permalink() ?>">
        <div class="item">
          <img class="w-100 h-100" src="<?php echo $thumb; ?>" alt="<?php echo $imageAlt ?>">
          <div class="overlay"></div>
          <p class="title d-flex align-items-center justify-content-center text-center w-100 h-100 mb-0 px-3"><?php the_title() ?></p>
        </div>
      </a>
		<?php endwhile; ?>
		<?php wp_reset_postdata(); ?>
  </div>

  <div class="container">
    <!-- latest news -->
    <div id="homepage-latest-news" class="latest-news py-3">
      <div class="w-100 mt-2 mb-3">
        <h2><?php the_field( 'homepage_latest_news_section_title' ) ?></h2>
        <a class="text-muted" href="<?php echo get_page_link( $news_page_id ); ?>"><?php echo $news_page_title ?></a>
      </div>

      <div class="row">
				<?php $homepage_latest_news_query = new WP_Query( 'posts_per_page=6&cat=4' );
				while ( $homepage_latest_news_query->have_posts() ) : $homepage_latest_news_query->the_post(); ?>

					<?php $image = get_field( 'homepage_latest_news_credit_cover_image' );
					if ( ! empty( $image ) ):
						$size  = 'medium';
						$thumb = $image['sizes'][ $size ];
						?>
					<?php endif; ?>

          <div class="col-md-6 col-lg-4 my-2">
            <div class="card h-100">
              <a href="<?php the_permalink() ?>">
                <img class="card-img-top" src="<?php echo $thumb; ?>" alt="<?php echo $imageAlt ?>">
              </a>
              <div class="card-body d-flex flex-column">
                <a class="text-dark" href="<?php the_permalink() ?>"><h5 class="mb-0"><?php the_title() ?></h5></a>
                <p class="card-text">
                  <small class="text-muted"><?php echo get_the_date(); ?></small>
                </p>
                <span class="text-muted"><?php the_field( 'homepage_latest_news_credit_short_description' ) ?></span>
                <a href="<?php the_permalink() ?>" class="btn btn-warning mr-auto mt-auto"><?php echo $news_read_more ?></a>
              </div>
            </div>
          </div>

				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
      </div>
    </div>
  </div>
</main>

<?php get_footer(); ?>
