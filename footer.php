<?php if ( 'en_US' == get_locale() ): ?>
	<?php
	$copyright             = 'All rights reserved, "Premium Credit" UCO LLC <i class="fa fa-copyright" aria-hidden="true"></i>';
	$footer_address        = get_field( 'main_en_address', 'options' );
	$contacts_page_id      = 189;
	$subscription_title    = 'Subscribe to our newsletter';
	$subscription_subtitle = 'Be the first to learn about our new offers and services.';
	$subscribe_form        = do_shortcode( '[contact-form-7 id="219" title="Subscribe"]' );
	?>
<?php endif; ?>

<?php if ( 'ru_RU' == get_locale() ): ?>
	<?php
	$copyright             = 'Все права защищены, ЗАО УКО "Премиум Кредит" <i class="fa fa-copyright" aria-hidden="true"></i>';
	$footer_address        = get_field( 'main_ru_address', 'options' );
	$contacts_page_id      = 188;
	$subscription_title    = 'Подпишитесь на нашу рассылку';
	$subscription_subtitle = 'Получайте наши новые предложения и услуги первыми.';
	$subscribe_form        = do_shortcode( '[contact-form-7 id="220" title="Подписаться"]' );
	?>
<?php endif; ?>

<?php if ( 'hy' == get_locale() ): ?>
	<?php
	$copyright             = 'Բոլոր իրավունքները պաշտպանված են, «Պրեմիում Կրեդիտ» ՈՒՎԿ ՓԲԸ <i class="fa fa-copyright" aria-hidden="true"></i>';
	$footer_address        = get_field( 'main_address', 'options' );
	$contacts_page_id      = 186;
	$subscription_title    = 'Բաժանորդագրվեք նորություններին';
	$subscription_subtitle = 'Առաջինը իմացեք մեր նոր ակցիաների և ծառայությունների մասին։';
	$subscribe_form        = do_shortcode( '[contact-form-7 id="218" title="Բաժանորդագրվել"]' );
	?>
<?php endif; ?>

<div class="subscription-section bg-faded">
  <div class="container py-6">
    <div class="row align-items-center">
      <div class="col-lg-5 text-center text-lg-left mb-2 mb-lg-0">
        <h5 class="mb-0"><?php echo $subscription_title ?></h5>
        <p class="text-muted small mb-0"><?php echo $subscription_subtitle ?></p>
      </div>
      <div class="col-lg-6 ml-md-auto">
				<?php echo $subscribe_form ?>
      </div>
    </div>
  </div>
</div>

<footer class="pt-3">
  <div class="container">
    <div class="row">
      <div class="col-4 col-sm-3 col-lg-2">
        <img class="footer-logo img-fluid px-md-2" src="<?php echo get_template_directory_uri(); ?>/src/img/logo-bw.svg" alt="<?php echo $imageAlt ?>">
      </div>
      <div class="col d-flex align-items-center">
        <ul class="list-inline small mb-0 ml-auto">
          <li class="list-inline-item mr-1">
            <a class="p-1" href="<?php echo get_page_link( $contacts_page_id ); ?>">
              <i class="fa fa-lg fa-fw fa-map-marker text-warning mr-lg-1 px-sm-1 px-lg-0 " aria-hidden="true"></i>
              <span class="d-none d-lg-inline-block"><?php echo $footer_address ?></span>
            </a>
          </li>
          <li class="list-inline-item mr-1">
            <a class="p-1" href="tel:0037411290000">
              <i class="fa fa-lg fa-fw fa-phone text-warning mr-lg-1 px-sm-1 px-lg-0" aria-hidden="true"></i>
              <span class="d-none d-lg-inline-block"><?php the_field( 'main_tel', 'options' ) ?></span>
            </a>
          </li>
          <li class="list-inline-item mr-md-1">
            <a class="p-1" href="mailto:<?php the_field( 'main_email', 'options' ) ?>">
              <i class="fa fa-lg fa-fw fa-envelope text-warning mr-lg-1 px-sm-1 px-lg-0" aria-hidden="true"></i>
              <span class="d-none d-xl-inline-block"><?php the_field( 'main_email', 'options' ) ?></span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>

  <div class="bottom-footer bg-dark text-uppercase mt-3 py-2 py-md-0">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-7 col-md">
					<?php if ( 'en_US' == get_locale() ): ?>
            <p class="mb-0"><?php echo $copyright ?><?php echo date( 'Y' ); ?></p>
					<?php endif; ?>
					<?php if ( 'ru_RU' == get_locale() ): ?>
            <p class="mb-0"><?php echo $copyright ?><?php echo date( 'Y' ); ?></p>
					<?php endif; ?>
					<?php if ( 'hy' == get_locale() ): ?>
            <p class="mb-0"><?php echo $copyright ?><?php echo date( 'Y' ); ?></p>
					<?php endif; ?>
        </div>

        <div class="col-5 col-md-3 col-lg-2">
          <a class="enero-logo d-block" href="http://www.enero-studio.com/" target="_blank">
            <img class="img-fluid py-1 px-2" src="<?php echo get_template_directory_uri(); ?>/src/img/enero-logo.svg" alt="<?php echo $imageAlt ?>">
          </a>
        </div>
      </div>
    </div>
  </div>
</footer>

<?php if ( is_page_template( 'page-contacts.php' ) ) : ?>
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBQdzihLRrRxZh3UxYLiFAYvzoOj0iN3BE&callback=initMap">
  </script>
<?php endif ?>

<?php wp_footer(); ?>
</body>
</html>
