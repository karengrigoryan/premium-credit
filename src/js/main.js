jQuery(document).ready(function ($) {
    // homepage carousel
    $('#homepage-carousel').owlCarousel({
        rewind: true,
        items: 1,
        nav: true,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        dots: false,
        animateOut: 'fadeOut',
        responsive: {
            768: {
                autoplay: true,
                autoplayTimeout: 5000
            }
        }
    });

    // homepage credits carousel
    $('.credits-carousel').owlCarousel({
        rewind: true,
        items: 1,
        nav: true,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        dots: false,
        autoplay: true,
        autoplayTimeout: 6000,
        responsive: {
            768: {
                items: 2
            },
            992: {
                items: 3
            }
        }
    });

    // hide navbar on on scroll down
    var didScroll;
    var lastScrollTop = 0;
    var delta = 50;
    var navbarHeight = 500;

    $(window).scroll(function (event) {
        didScroll = true;
    });

    setInterval(function () {
        if (didScroll) {
            hasScrolled();
            didScroll = false;
        }
    }, 250);

    function hasScrolled() {
        var st = $(this).scrollTop();

        if (Math.abs(lastScrollTop - st) <= delta)
            return;

        if (st > lastScrollTop && st > navbarHeight) {
            $('.main-navbar').addClass('window-scrolled');
        } else {
            if (st + $(window).height() < $(document).height()) {
                $('.main-navbar').removeClass('window-scrolled');
            }
        }
        lastScrollTop = st;
    }

    /* loan calculator */
    var loanCalculator = $('#loan-calculator-page'),

        // global options
        loanMaxMonths = 60,

        // buttons
        loanCalculatorResetButton = $('#loan-calculator-reset'),
        loanCalculatorCalculateButton = $('#loan-calculator-calculate'),

        // inputs
        loanAmount = $('#loan-amount'),
        loanMonths = $('#loan-months'),
        loanRate = $('#loan-rate'),
        loanExtraPayment = $('#loan-extra-payments'),

        // areas
        loanErrors = $('#loan-calculator-errors-container'),
        loanInformation = $('#loan-information'),
        loanScheduleArea = $('#loan-schedule-area'),
        loanTable = $('#loan-table');

    // buttons behaviour
    loanCalculatorResetButton.click(function () {
        $(document).scrollTop( loanInformation.offset().top -300 );
        loanErrors.find('.alert').addClass('d-none');
        loanCalculator.find('input').val('');
        loanExtraPayment.val(0);

        loanTable.html('');
        loanInformation.addClass('d-none');
        loanScheduleArea.addClass('d-none')
    });

    loanCalculatorCalculateButton.click(function () {
        if (loanAmount.val() <= 0) {
            $(document).scrollTop( loanErrors.offset().top -150 );
            loanErrors.find('.alert').addClass('d-none');
            $('#loan-calculate-amount-error').removeClass('d-none');
            loanAmount.val('')
        }

        else if (loanMonths.val() <= 0 || loanMonths.val() > loanMaxMonths) {
            $(document).scrollTop( loanErrors.offset().top -150 );
            loanErrors.find('.alert').addClass('d-none');
            $('#loan-calculate-months-error').removeClass('d-none');
            loanMonths.val('')
        }

        else if (loanRate.val() <= 0 || loanRate.val() > 24) {
            $(document).scrollTop( loanErrors.offset().top -150 );
            loanErrors.find('.alert').addClass('d-none');
            $('#loan-calculate-rate-error').removeClass('d-none');
            loanRate.val('')
        }

        else if (loanExtraPayment.val() < 0) {
            $(document).scrollTop( loanErrors.offset().top -150 );
            loanErrors.find('.alert').addClass('d-none');
            $('#loan-calculate-extra-payment-error').removeClass('d-none');
            loanAmount.val(0)
        }

        else {
            $(document).scrollTop( loanInformation.offset().top -300 );
            loanErrors.find('.alert').addClass('d-none');
            loanTable.html('');
            calculate(parseFloat(loanAmount.val()), parseInt(loanMonths.val()), parseFloat(loanRate.val()), parseFloat(loanExtraPayment.val()));

            loanInformation.removeClass('d-none');
            loanScheduleArea.removeClass('d-none')
        }
    });

    // rounding number to short num.
    function roundNumber(num, dec) {
        return (Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec)).toFixed(dec)
    }

    // adding commas to numbers
    function digits(val) {
        while (/(\d+)(\d{3})/.test(val.toString())) {
            val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
        }
        return val;
    }

    // main calculations
    function calculate(amount, months, rate, extra_payment) {
        var i = rate / 100,
            monthly_payment = amount * (i / 12) * Math.pow((1 + i / 12), months) / (Math.pow((1 + i / 12), months) - 1);

        // loan information area
        $('#loan-details-amount').html(digits(amount));
        $('#loan-details-months').html(months);
        $('#loan-details-percentage').html(rate);
        $('#loan-details-extra-payment').html(digits(extra_payment));
        $('#loan-details-full-monthly-payment').html(digits(roundNumber(monthly_payment + extra_payment, 2)));

        monthly_payment = monthly_payment + extra_payment;

        // loan extended table
        var paymentCounter = 1;

        while (amount > 0) {
            var towardsInterest = (i / 12) * amount;
            if (monthly_payment > amount) {
                monthly_payment = amount + towardsInterest
            }
            var towardsBalance = monthly_payment - towardsInterest;
            amount = amount - towardsBalance;

            loanTable.append('<tr>');
            loanTable.append('<td>' + paymentCounter + '</td>');
            loanTable.append('<td>' + digits(roundNumber(monthly_payment, 2)) + '</td>');
            loanTable.append('<td>' + digits(roundNumber(towardsBalance, 2)) + '</td>');
            loanTable.append('<td>' + digits(roundNumber(towardsInterest, 2)) + '</td>');
            loanTable.append('<td>' + digits(extra_payment) + '</td>');
            loanTable.append('<td>' + digits(roundNumber(amount, 2)) + '</td>');
            loanTable.append('</tr>');

            paymentCounter++
        }
    }

    // exporting table to print-ready page
    $("#loan-calculator-print-button").live("click", function () {
        var loanScheduleAreaTitle = $("#loan-schedule-area-title").html();
        var loanCalculatorPrintContent = $("#loan-table-container").html();
        var loanCalculatorPrintWindow = window.open('', '', 'height=400,width=800');
        loanCalculatorPrintWindow.document.write('<html><head>' +
            '<title>Premium Credit</title>' +
            '<link rel="stylesheet" id="main-css" href="//localhost:3000/best-credit/wp-content/themes/platinum-by-enero-studio/style.css?ver=4.8" type="text/css" media="all">');
        loanCalculatorPrintWindow.document.write('</head><body>');
        loanCalculatorPrintWindow.document.write('<h3 class="text-center my-3">' + loanScheduleAreaTitle + '</h3>');
        loanCalculatorPrintWindow.document.write('<div>' + loanCalculatorPrintContent + '</div>');
        loanCalculatorPrintWindow.document.write('</body></html>');
        loanCalculatorPrintWindow.document.close();
        setTimeout(function () {
            loanCalculatorPrintWindow.print();
        }, 1000);
    });

    // doubleTapToGo
    $('#main-menu').doubleTapToGo();

}); // /document ready
