<?php get_header(); ?>

<?php if ( 'en_US' == get_locale() ): ?>
	<?php
	$credit_terms                     = 'Terms';
	$other_credits_title              = 'Other loans';
	$credit_form                      = '[contact-form-7 id="383" title="Վարկի օնլայն դիմում-հայտ"]';
	$credit_modal_button_label        = 'Apply now!';
	$credit_modal_description         = 'Fill out online application form and our specialists will contact you shortly.';
	$credit_modal_title               = 'Loan online application';
	$credit_modal_close_button_label  = 'Close';
	$credit_informational_paper_title = 'Informational document';
	?>
<?php endif; ?>

<?php if ( 'ru_RU' == get_locale() ): ?>
	<?php
	$credit_terms                     = 'Условия';
	$other_credits_title              = 'Другие кредиты';
	$credit_form                      = '[contact-form-7 id="383" title="Վարկի օնլայն դիմում-հայտ"]';
	$credit_modal_button_label        = 'Online заявка';
	$credit_modal_description         = 'Запонтие online заявку и наши специалисты скоро свяжутся с Вами.';
	$credit_modal_title               = 'Online заявка на кредит';
	$credit_modal_close_button_label  = 'Закрыть';
	$credit_informational_paper_title = 'Информационный листок';
	?>
<?php endif; ?>

<?php if ( 'hy' == get_locale() ): ?>
	<?php
	$credit_terms                     = 'Պայմաններ';
	$other_credits_title              = 'Այլ վարկեր';
	$credit_form                      = '[contact-form-7 id="383" title="Վարկի օնլայն դիմում-հայտ"]';
	$credit_modal_button_label        = 'Դիմել online';
	$credit_modal_description         = 'Լրացրեք online հայտը և մեր մասնագետները հնարավորինս արագ կկապվեն Ձեզ։';
	$credit_modal_title               = 'Վարկի online դիմում';
	$credit_modal_close_button_label  = 'Փակել';
	$credit_informational_paper_title = 'Ընդհանրական թերթիկը';
	?>
<?php endif; ?>

<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>
    <main class="d-flex flex-column bg-white">
      <div class="page-header credit-page-header">
				<?php $image = get_field( 'credit_cover_image' ) ?>

        <img class="page-cover-image w-100 h-100" src="<?php echo $image['url']; ?>" alt="<?php echo $imageAlt ?>">

        <div class="page-information d-flex flex-column align-items-center justify-content-center">
          <h2 class="title d-flex align-items-center justify-content-center text-center text-white px-3 mb-0 w-100"><?php the_title() ?></h2>
          <div class="credit-information w-100 mt-1 small d-flex align-items-center justify-content-center">
            <h5 class="mb-0 p-1 mr-1 rounded font-weight-normal"><?php the_field( 'credit_percentage' ) ?>%</h5>

            <h5 class="mb-0 p-1 rounded font-weight-normal"><?php the_field( 'credit_period' ) ?></h5>
          </div>
        </div>
      </div>

      <div class="container mt-4">
        <div class="row">
					<?php $description = get_field( 'credit_long_description' ); if ( ! empty( $description ) ): ?>
            <div class="col-md-8 col-lg-9">
							<?php the_field( 'credit_long_description' ) ?>

              <?php if (is_single( array( 108 ) )): ?>
                <table class="table table-responsive table-bordered">
                  <tbody>
                    <tr>
                      <td>375`</td>
                      <td><?php the_field('gold_rate_375', 'option') ?> ՀՀ դրամ</td>
                    </tr>
                    <tr>
                      <td>500`</td>
                      <td><?php the_field('gold_rate_500', 'option') ?> ՀՀ դրամ</td>
                    </tr>
                    <tr>
                      <td>560`</td>
                      <td><?php the_field('gold_rate_560', 'option') ?> ՀՀ դրամ</td>
                    </tr>
                    <tr>
                      <td>583`</td>
                      <td><?php the_field('gold_rate_583', 'option') ?> ՀՀ դրամ</td>
                    </tr>
                    <tr>
                      <td>750`</td>
                      <td><?php the_field('gold_rate_750', 'option') ?> ՀՀ դրամ</td>
                    </tr>
                    <tr>
                      <td>875`</td>
                      <td><?php the_field('gold_rate_875', 'option') ?> ՀՀ դրամ</td>
                    </tr>
                    <tr>
                      <td>900`</td>
                      <td><?php the_field('gold_rate_900', 'option') ?> ՀՀ դրամ</td>
                    </tr>
                    <tr>
                      <td>958 և ավել`</td>
                      <td><?php the_field('gold_rate_958', 'option') ?> ՀՀ դրամ</td>
                    </tr>
                  </tbody>
                </table>

                <p class="small mt-1 mb-0">«Սպիտակ Ոսկու» գրավով վարկերի դեպքում ոսկու 1 գրամի արժեքը գնահատվում է դեղին ոսկու համապատասխան հարգի 1 գրամից 20 % ցածր արժեքով:</p>
              <?php endif; ?>
            </div>
					<?php endif; ?>

          <div class="col-md-4 col-lg-3">
            <button class="btn btn-lg btn-block btn-success" data-toggle="modal" data-target="#credit-modal"><?php echo $credit_modal_button_label; ?></button>

            <p class="text-muted small mt-1 mb-0"><?php echo $credit_modal_description ?></p>

            <div class="modal fade credit-modal" id="credit-modal" tabindex="-1" role="dialog"
                aria-labelledby="credit-modal-header" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header p-0">
                    <img class="w-100 h-100" src="<?php echo $image['url']; ?>" alt="<?php echo $imageAlt ?>">

                    <h2 class="modal-title text-white d-flex align-items-center justify-content-center" id="credit-modal-header"><?php echo $credit_modal_title ?></h2>

                    <button type="button" class="close text-white p-1" data-dismiss="modal" aria-label="<?php echo $credit_modal_close_button_label ?>">
                      <span aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i></span>
                    </button>
                  </div>

                  <div class="modal-body p-4">
										<?php echo do_shortcode( $credit_form ); ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <hr class="my-4">

        <h3 class="mt-4"><?php echo $credit_terms ?></h3>

        <div class="text-muted small mb-4">
					<?php the_field( 'credit_terms_note' ) ?>
        </div>

				<?php the_field( 'credit_terms' ) ?>

				<?php if ( get_field( 'credit_informational_paper_link' ) ): ?>
          <div class="text-center mt-2 mb-6">
            <a href="<?php the_field( 'credit_informational_paper_link' ) ?>" class="btn btn-warning btn-lg col-12 col-lg-6" target="_blank" download><?php echo $credit_informational_paper_title ?></a>
          </div>
				<?php endif; ?>
      </div>

      <h4 class="text-center mb-3 mt-auto"><?php echo $other_credits_title ?></h4>

      <div class="credits-carousel owl-carousel">
				<?php $currentID        = get_the_ID();
				$homepage_credits_query = new WP_Query( array(
						'posts_per_page' => 10,
						'post__not_in'   => array( $currentID ),
						'cat'            => 3
				) );
				while ( $homepage_credits_query->have_posts() ) : $homepage_credits_query->the_post(); ?>

					<?php $image = get_field( 'credit_cover_image' ); if ( ! empty( $image ) ):
						$size  = 'medium';
						$thumb = $image['sizes'][ $size ];
						?>
					<?php endif; ?>

          <a href="<?php the_permalink() ?>">
            <div class="item">
              <img class="w-100 h-100" src="<?php echo $thumb; ?>" alt="<?php echo $imageAlt ?>">

              <div class="overlay"></div>

              <p class="title d-flex align-items-center justify-content-center text-center w-100 h-100 mb-0 px-3"><?php the_title() ?></p>
            </div>
          </a>

				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
      </div>
    </main>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
