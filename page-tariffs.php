<?php
/* Template Name: Tariffs */
?>
<?php get_header(); ?>

<?php if ( 'en_US' == get_locale() ): ?>
	<?php
	$tariffs_notice_text = 'Հաստատված է «Պրեմիում Կրեդիտ» ՈՒՎԿ ՓԲԸ-ի 03.08.2017թ. թիվ 1 վարչության որոշմամբ Ուժի մեջ է ընդունման պահից։';
	?>
<?php endif; ?>

<?php if ( 'ru_RU' == get_locale() ): ?>
	<?php
	$tariffs_notice_text = 'Հաստատված է «Պրեմիում Կրեդիտ» ՈՒՎԿ ՓԲԸ-ի 03.08.2017թ. թիվ 1 վարչության որոշմամբ Ուժի մեջ է ընդունման պահից։';
	?>
<?php endif; ?>

<?php if ( 'hy' == get_locale() ): ?>
	<?php
	$tariffs_notice_text = 'Հաստատված է «Պրեմիում Կրեդիտ» ՈՒՎԿ ՓԲԸ-ի 03.08.2017թ. թիվ 1 վարչության որոշմամբ Ուժի մեջ է ընդունման պահից։';
	?>
<?php endif; ?>

<main class="page-content bg-white">
  <div class="page-header bg-dark">
    <div class="d-flex flex-column align-items-center justify-content-center py-7">
      <h2 class="title text-center text-white mb-0 px-3"><?php the_title() ?></h2>
    </div>
  </div>

  <div class="container mt-5">
		<?php the_field( 'page_content' ) ?>

    <p class="small text-muted mb-6">
		  <?php echo $tariffs_notice_text ?>
    </p>
  </div>
</main>

<?php get_footer(); ?>
