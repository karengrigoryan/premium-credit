<?php /* Template Name: Calculators */ ?>
<?php get_header(); ?>

<?php if ( 'en_US' == get_locale() ): ?>
	<?php
	$loanAmount             = 'Loan amount';
	$loanMonths             = 'Months';
	$loanRate               = 'Yearly rate';
	$loanExtraPayments      = 'Extra monthly payment/s';
	$loanExtraPaymentsShort = 'Extra payment/s';
	$loanMonthlyPayment     = 'Monthly payment';

	$loanMonth           = 'month/s';
	$loanCalculateButton = 'Calculate';
	$loanResetButton     = 'Reset';

	$loanSchedule          = 'Schedule';
	$loanSchedulePrint     = 'Print';
	$loanMainMonthlyAmount = 'Main amount';
	$loanPercentage        = 'Percentage';
	$loanBalance           = 'Balance';

	$loanCalculateAmountError       = 'Wrong information in <strong>"' . $loanAmount . '"</strong> field. Please use only digits.';
	$loanCalculateMonthsError       = 'Wrong information in <strong>"' . $loanMonths . '"</strong>. Please use only digits.';
	$loanCalculateRateError         = 'Wrong information in <strong>"' . $loanRate . '"</strong>. Please use only digits without percent symbol (%).';
	$loanCalculateExtraPaymentError = 'Wrong information in <strong>"' . $loanExtraPayments . '"</strong>. Please use only digits.'
	?>
<?php endif; ?>

<?php if ( 'ru_RU' == get_locale() ): ?>
	<?php
	$loanAmount             = 'Сумма кредита';
	$loanMonths             = 'Количество месяцев';
	$loanRate               = 'Годовая процентная ставка';
	$loanExtraPayments      = 'Дополнительные ежемесячные платеж/и';
	$loanExtraPaymentsShort = 'Дополнительные платеж/и';
	$loanMonthlyPayment     = 'Ежемесячьный платеж';

	$loanMonth           = 'месяц/ев';
	$loanCalculateButton = 'Рассчитать';
	$loanResetButton     = 'Отчистить';

	$loanSchedule          = 'Ժամանակացույց';
	$loanSchedulePrint     = 'Տպել';
	$loanMainMonthlyAmount = 'Մայր գումար';
	$loanPercentage        = 'Տոկոս';
	$loanBalance           = 'Մնացորդ';

	$loanSchedule          = 'График выплат';
	$loanSchedulePrint     = 'Печать';
	$loanMainMonthlyAmount = 'Основная сумма';
	$loanPercentage        = 'Процент';
	$loanBalance           = 'Остаток';

	$loanCalculateAmountError       = 'Неправильно заполнено поле <strong>"' . $loanAmount . '"</strong>. Пожалуйста вводите только цифры.';
	$loanCalculateMonthsError       = 'Неправильно заполнено поле <strong>"' . $loanMonths . '"</strong>. Пожалуйста вводите только цифры.';
	$loanCalculateRateError         = 'Неправильно заполнено поле <strong>"' . $loanRate . '"</strong>. Пожалуйста вводите только цифры без знака процента (%).';
	$loanCalculateExtraPaymentError = 'Неправильно заполнено поле <strong>"' . $loanExtraPayments . '"</strong>. Пожалуйста вводите только цифры.'
	?>
<?php endif; ?>

<?php if ( 'hy' == get_locale() ): ?>
	<?php
	$loanAmount             = 'Վարկի գումար';
	$loanMonths             = 'Ամիսների քանակ';
	$loanRate               = 'Տարեկան տոկոսադրույք';
	$loanExtraPayments      = 'Հավելյալ ամսական վճար/ներ';
	$loanExtraPaymentsShort = 'Հավելյալ վճար/ներ';
	$loanMonthlyPayment     = 'Ամսական վճար';

	$loanMonth           = 'ամիս';
	$loanCalculateButton = 'Հաշվարկել';
	$loanResetButton     = 'Մաքրել';

	$loanSchedule          = 'Ժամանակացույց';
	$loanSchedulePrint     = 'Տպել';
	$loanMainMonthlyAmount = 'Մայր գումար';
	$loanPercentage        = 'Տոկոս';
	$loanBalance           = 'Մնացորդ';

	$loanCalculateAmountError       = 'Սխալ է լրացված <strong>«' . $loanAmount . '»</strong> դաշտը։ Խնդրում ենք օգտագործել միայն թվանշաններ։';
	$loanCalculateMonthsError       = 'Սխալ է լրացված <strong>«' . $loanMonths . '»</strong> դաշտը։ Խնդրում ենք օգտագործել միայն թվանշաններ։';
	$loanCalculateRateError         = 'Սխալ է լրացված <strong>«' . $loanRate . '»</strong> դաշտը։ Խնդրում ենք օգտագործել միայն թվանշաններ առանց տոկոսի (%) նշանի։';
	$loanCalculateExtraPaymentError = 'Սխալ է լրացված <strong>«' . $loanExtraPayments . '»</strong> դաշտը։ Խնդրում ենք օգտագործել միայն թվանշաններ։' ?>
<?php endif; ?>

<main id="loan-calculator-page" class="bg-faded pb-8">
  <div class="page-header">
    <div class="d-flex flex-column align-items-center justify-content-center bg-dark py-7">
      <h2 class="title text-center text-white mb-0 px-3"><?php the_title() ?></h2>
    </div>
  </div>

  <div class="container">
    <div class="my-5">
			<?php the_field( 'page_intro_text' ) ?>
    </div>

    <div class="row">
      <div class="col-xl">
        <div id="loan-calculator-errors-container" class="small">
          <div id="loan-calculate-amount-error" class="d-none alert alert-danger mb-2" role="alert">
            <i class="fa fa-lg fa-exclamation mr-1" aria-hidden="true"></i>
						<?php echo $loanCalculateAmountError ?>
          </div>
          <div id="loan-calculate-months-error" class="d-none alert alert-danger mb-2" role="alert">
            <i class="fa fa-lg fa-exclamation mr-1" aria-hidden="true"></i>
						<?php echo $loanCalculateMonthsError ?>
          </div>
          <div id="loan-calculate-rate-error" class="d-none alert alert-danger mb-2" role="alert">
            <i class="fa fa-lg fa-exclamation mr-1" aria-hidden="true"></i>
						<?php echo $loanCalculateRateError ?>
          </div>
          <div id="loan-calculate-extra-payment-error" class="d-none alert alert-danger mb-2" role="alert">
            <i class="fa fa-lg fa-exclamation mr-1" aria-hidden="true"></i>
						<?php echo $loanCalculateExtraPaymentError ?>
          </div>
        </div>

        <form>
          <div class="form-group">
            <label for="loan-amount"><?php echo $loanAmount ?></label>
            <div class="input-group">
              <input type="number" min="1" class="form-control" id="loan-amount" placeholder="1500000">
              <div class="input-group-addon justify-content-center col-3">
                <img src="<?php echo get_template_directory_uri() ?>/src/img/dram.svg" alt="Premium Credit" width="18" class="mx-auto">
              </div>
            </div>
          </div>

          <div class="form-group">
            <label for="loan-months"><?php echo $loanMonths ?></label>
            <div class="input-group">
              <input type="number" min="1" max="60" step="1" class="form-control" id="loan-months" placeholder="12">
              <div class="input-group-addon justify-content-center col-3"><?php echo $loanMonth ?></div>
            </div>
          </div>

          <div class="form-group">
            <label for="loan-rate"><?php echo $loanRate ?></label>
            <div class="input-group">
              <input type="number" min="1" max="24" step="1" class="form-control" id="loan-rate" placeholder="24">
              <div class="input-group-addon justify-content-center col-3">%</div>
            </div>
          </div>

          <div class="form-group">
            <label for="loan-extra-payments"><?php echo $loanExtraPayments ?></label>
            <div class="input-group">
              <input type="number" min="1" class="form-control" id="loan-extra-payments" name="loan_extra_payments" value="0">
              <div class="input-group-addon justify-content-center col-3">
                <img src="<?php echo get_template_directory_uri() ?>/src/img/dram.svg" alt="Premium Credit" width="18" class="mx-auto">
              </div>
            </div>
          </div>

          <div class="mt-3">
            <button id="loan-calculator-calculate" type="button" class="btn btn-lg btn-primary mr-1"><?php echo $loanCalculateButton ?>
            </button>
            <button id="loan-calculator-reset" type="button" class="btn btn-lg btn-warning"><?php echo $loanResetButton ?></button>
          </div>
        </form>
      </div>

      <div id="loan-information" class="col-xl offset-xl-1 d-none">
        <ul class="list-group pt-3">
          <li class="list-group-item d-flex align-items-center">
						<?php echo $loanAmount ?>
            <p id="loan-details-amount" class="font-weight-bold ml-auto mb-0"></p>
            <img src="<?php echo get_template_directory_uri() ?>/src/img/dram.svg" alt="Premium Credit" width="12">
          </li>
          <li class="list-group-item d-flex align-items-center">
						<?php echo $loanMonths ?>
            <p id="loan-details-months" class="font-weight-bold ml-auto mb-0"></p>
          </li>
          <li class="list-group-item d-flex align-items-center">
						<?php echo $loanRate ?>
            <p id="loan-details-percentage" class="font-weight-bold ml-auto mb-0"></p>%
          </li>
          <li class="list-group-item d-flex align-items-center">
						<?php echo $loanExtraPayments ?>
            <p id="loan-details-extra-payment" class="font-weight-bold ml-auto mb-0"></p>
            <img src="<?php echo get_template_directory_uri() ?>/src/img/dram.svg" alt="Premium Credit" width="12">
          </li>
          <li class="list-group-item d-flex align-items-center">
						<?php echo $loanMonthlyPayment ?>
            <p id="loan-details-full-monthly-payment" class="font-weight-bold ml-auto mb-0"></p>
            <img src="<?php echo get_template_directory_uri() ?>/src/img/dram.svg" alt="Premium Credit" width="12">
          </li>
        </ul>
      </div>

      <div id="loan-schedule-area" class="col-12 d-none">
        <hr class="mt-5">
        <div class="d-flex align-items-center mt-5">
          <h3 id="loan-schedule-area-title text-truncate" class="text-center mb-0"><?php echo $loanSchedule ?></h3>
          <button id="loan-calculator-print-button" type="button" class="btn btn-primary ml-auto">
            <i class="fa fa-lg fa-print mr-1" aria-hidden="true"></i>
						<?php echo $loanSchedulePrint ?>
          </button>
        </div>

        <div id="loan-table-container" class="table-responsive mt-6">
          <table class="table table-bordered bg-white">
            <thead>
              <tr>
                <th id="loan-table-payment-number">#</th>
                <th id="loan-table-payment" class="text-truncate"><?php echo $loanMonthlyPayment ?></th>
                <th id="loan-payment-main-num" class="text-truncate"><?php echo $loanMainMonthlyAmount ?></th>
                <th id="loan-payment-main-percents" class="text-truncate"><?php echo $loanPercentage ?></th>
                <th id="loan-payment-extra-num" class="text-truncate"><?php echo $loanExtraPaymentsShort ?></th>
                <th id="loan-payment-balance" class="text-truncate"><?php echo $loanBalance ?></th>
              </tr>
            </thead>

            <tbody id="loan-table"></tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</main><?php get_footer(); ?>
