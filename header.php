<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no">
    <meta name="author" content="Premium Credit UCO LLC">
    <meta name="keywords"
        content="premium,credit,finance,accounts,money,loans,armenia,yerevan,պրեմիում,կրեդիտ,ֆինանսներ,հաշիվներ,գումար,վարկեր,հայաստան,երևան,премиум,кредит,финансы,счета,кредиты,армения,ереван">
		<?php if ( 'en_US' == get_locale() ): ?>
      <meta name="description" content="Loans in one day without any income and guarantee person. Secured by jewelry, cars and real estate. str. Buzand 1/3, (011 29 00 00):"/>
		<?php endif; ?>
		<?php if ( 'ru_RU' == get_locale() ): ?>
      <meta name="description" content="Кредиты в один день без какого-либо доходов и гарантий. Кредиты обеспечены ювелирными изделиями, автомобилями и недвижимостью. ул. Бузанда 1/3, (011 29 00 00):"/>
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
		<?php endif; ?>
		<?php if ( 'hy' == get_locale() ): ?>
      <meta name="description" content="Վարկեր մեկ օրում առանց եկամուտների հիմնավորմամբ, առանց երաշխավորի: Ոսկյա իրերի, ավտոմեքենայի և անշարժ գույքի գրավադրմամբ: Բուզանդի 1/3շ. (011 29 00 00):"/>
		<?php endif; ?>
    <meta name="format-detection" content="telephone=no">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/favicon-16x16.png">
    <link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/manifest.json">
    <link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">
    <meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/src/img/premium-credit-og.jpg"/>
    <title>
			<?php if ( is_home() ) {
				bloginfo( 'name' );
			} elseif ( is_category() ) {
				single_cat_title();
				echo ' - ';
				bloginfo( 'name' );
			} elseif ( is_page() ) {
				single_post_title();
				echo ' | ';
				bloginfo( 'name' );
			} else {
				wp_title( '', true );
			} ?>
    </title>
		<?php wp_head(); ?>
  </head>

  <!-- variables -->
	<?php if ( 'en_US' == get_locale() ): ?>
		<?php
		$currencies_section_title      = 'Exchange rates';
		$currencies_section_buy_title  = 'Buy';
		$currencies_section_sell_title = 'Sell';
		$languages_section_heading     = 'Language';
		$imageAlt                      = 'Premium Credit';
		?>
	<?php endif; ?>

	<?php if ( 'ru_RU' == get_locale() ): ?>
		<?php
		$currencies_section_title      = 'Курсы валют';
		$currencies_section_buy_title  = 'Купля';
		$currencies_section_sell_title = 'Продажа';
		$languages_section_heading     = 'Язык';
		$imageAlt                      = 'Премиум Кредит';
		?>
	<?php endif; ?>

	<?php if ( 'hy' == get_locale() ): ?>
		<?php
		$currencies_section_title      = 'Արտարժույթի փոխարժեքներ';
		$currencies_section_buy_title  = 'Առք';
		$currencies_section_sell_title = 'Վաճառք';
		$languages_section_heading     = 'Լեզուներ';
		$imageAlt                      = 'Պրեմիում Կրեդիտ';
		?>
	<?php endif; ?>

  <body class="d-flex flex-column <?php if ( 'ru_RU' == get_locale() ): ?>lang-ru<?php endif; ?>">
    <!-- logo and language-bar -->
    <header class="position-relative">
      <div id="main-logo" class="bg-white text-center pt-3 pt-xl-0">
        <a href="<?php echo get_home_url(); ?>" class="d-block px-2 px-lg-3 px-xl-2">
          <img class="py-xl-2 px-xl-1" src="<?php echo get_template_directory_uri(); ?>/src/img/logo.svg" alt="<?php echo $imageAlt ?>">
        </a>
        <div class="d-xl-none language-bar mt-2 mb-1">
					<?php echo do_shortcode( '[bogo]' ); ?>
        </div>
      </div>
    </header>

    <!-- navbar -->
    <nav class="main-navbar w-100 sticky-xl animated">
      <button id="mobile-navbar-toggler" type="button" name="navbar-mobile-toggler" class="btn btn-block bg-primary-gradient rounded-0 text-white d-xl-none" data-toggle="collapse" href="#nav-collapse" aria-expanded="false" aria-controls="nav-collapse">
        <i class="fa fa-bars" aria-hidden="true"></i></button>
      <div class="navbar-inner container-fluid">
        <div class="col-xl-9 collapse d-xl-flex w-100 ml-auto px-0 px-xl-3" id="nav-collapse">
          <!-- main menu -->
					<?php wp_nav_menu( array(
							'theme_location' => 'main',
							'container'      => false,
							'items_wrap'     => '<ul id="main-menu" class="pl-xl-1">%3$s</ul>'
					) ); ?>

          <!-- secondary actions -->
          <div class="d-flex flex-wrap flex-xl-nowrap align-items-center ml-auto">
            <!-- languages bar (desktop) -->
            <button class="btn btn-sm btn-link text-dark w-100 pl-0 pr-2 pr-lg-0 align-items-center text-left d-none d-xl-flex" type="button" name="language-bar-button" data-toggle="collapse" href="#d-language-bar" aria-expanded="false" aria-controls="language-bar">
              <i class="fa fa-lg fa-globe" aria-hidden="true"></i>
              <i class="fa fa-sm fa-chevron-down ml-1 text-muted" aria-hidden="true"></i>
            </button>

            <div class="collapse rounded-bottom language-bar w-auto" id="d-language-bar">
              <div class="p-1">
								<?php echo do_shortcode( '[bogo]' ); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </nav>

    <!-- exchange rates -->
    <div id="header-exchange-rates-container">
      <button id="exchange-section-mobile-toggler" data-toggle="collapse" data-target="#exchange-collapse" aria-expanded="false" aria-controls="exchange-collapse" type="button" class="btn btn-block btn-link d-md-none">
        <i class="fa fa-fw fa-usd" aria-hidden="true"></i>
      </button>

      <div id="exchange-collapse" class="container collapse d-md-block px-0">
        <div class="row align-items-center">
          <div class="col-xl-8 d-flex flex-wrap flex-md-nowrap align-items-center justify-content-center justify-content-xl-start ml-xl-auto">
            <p class="text-white text-center text-md-left w-sm-down-100 ml-xl-auto mb-0 py-2 py-md-0"><?php echo $currencies_section_title ?></p>

            <ul class="list-inline d-flex flex-column flex-md-row text-white ml-md-2 mb-2 mb-md-0 p-2 p-md-1">
              <li class="list-inline-item mx-auto ml-md-0 mr-md-2 mb-1 mb-md-0"><?php echo $currencies_section_buy_title ?></li>
              <li class="list-inline-item">
                <i class="fa fa-fw fa-usd" aria-hidden="true"></i>
								<?php the_field( 'usd_buy', 'option' ) ?>
              </li>
              <li class="list-inline-item ml-md-1">
                <i class="fa fa-fw fa-eur" aria-hidden="true"></i>
								<?php the_field( 'eur_buy', 'option' ) ?>
              </li>
              <li class="list-inline-item ml-md-1">
                <i class="fa fa-fw fa-rub" aria-hidden="true"></i>
								<?php the_field( 'rub_buy', 'option' ) ?>
              </li>
            </ul>

            <ul class="list-inline d-flex flex-column flex-md-row text-white ml-2 mb-2 mb-md-0 p-2 p-md-1">
              <li class="list-inline-item mx-auto ml-md-0 mr-md-2 mb-1 mb-md-0"><?php echo $currencies_section_sell_title ?></li>
              <li class="list-inline-item">
                <i class="fa fa-fw fa-usd" aria-hidden="true"></i>
								<?php the_field( 'usd_sale', 'option' ) ?>
              </li>
              <li class="list-inline-item ml-md-1">
                <i class="fa fa-fw fa-eur" aria-hidden="true"></i>
								<?php the_field( 'eur_sale', 'option' ) ?>
              </li>
              <li class="list-inline-item ml-md-1">
                <i class="fa fa-fw fa-rub" aria-hidden="true"></i>
								<?php the_field( 'rub_sale', 'option' ) ?>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
