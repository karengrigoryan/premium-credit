<?php /* Template Name: Management */ ?>
<?php get_header(); ?>

<main class="bg-faded pb-3 " id="management">
  <div class="page-header bg-dark">
    <div class="d-flex flex-column align-items-center justify-content-center py-7">
      <h2 class="title text-center text-white mb-0 px-3"><?php the_title() ?></h2>
    </div>
  </div>

  <div class="container">
    <div class="my-5">
			<?php the_field( 'page_intro_text' ) ?>
    </div>

    <div class="row">
			<?php if ( have_rows( 'page_management_members' ) ): ?>
				<?php while ( have_rows( 'page_management_members' ) ) : the_row(); ?>
					<?php $image = get_sub_field( 'photo' );
					if ( ! empty( $image ) ):
						$size  = 'medium';
						$thumb = $image['sizes'][ $size ];
						?>
					<?php endif; ?>

          <div class="card shadow w-100 flex-md-row mb-5 mx-2 py-4 px-2">
						<?php if ( $image ): ?>
              <div class="col-12 col-md-5 col-lg-3 text-center mb-2 mb-md-0">
                <img class="img-fluid rounded-circle shadow-xs user-avatar" src="<?php echo $thumb; ?>"
                    alt="<?php echo $imageAlt ?>">
              </div>
						<?php endif; ?>

            <div class="col-12 col-md">
              <p class="text-center text-md-left mb-0">
								<?php the_sub_field( 'position' ) ?>
              </p>

              <h4 class="text-center text-md-left mb-4 mb-md-1">
								<?php the_sub_field( 'name' ) ?>
              </h4>

              <div class="small text-muted mt-2">
								<?php the_sub_field( 'details' ) ?>
              </div>
            </div>
          </div>
				<?php endwhile; ?>
			<?php endif; ?>
    </div>
  </div>


</main>
<?php get_footer(); ?>
