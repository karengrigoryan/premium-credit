<?php

// Menu
add_theme_support( 'menus' );
function register_theme_menus() {
    register_nav_menus(
            array(
                'main' => __( 'Main Menu', 'elegant_by_enero' ),
            )
        );
    }
add_action( 'init', 'register_theme_menus' );
// ----------------------------------------------------------------------------------------

// CSS
function wpt_theme_styles() {
    wp_enqueue_style( 'main', get_template_directory_uri() . '/style.css' );
}
add_action( 'wp_enqueue_scripts', 'wpt_theme_styles' );
// ----------------------------------------------------------------------------------------

// JavaScipt
function wpt_theme_js() {
    wp_enqueue_script( 'popper.min.js', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js', array('jquery'), '', true );
    wp_enqueue_script( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js', array('jquery'), '', true );
    wp_enqueue_script( 'main', get_template_directory_uri() . '/src/js/main.js', array('jquery'), '', true );
    wp_enqueue_script( 'owlcarousel', get_template_directory_uri() . '/src/js/owl.carousel.min.js', array('jquery'), '', true );
    wp_enqueue_script( 'doubleTapToGo', get_template_directory_uri() . '/src/js/jquery.dcd.doubletaptogo.min.js', array('jquery'), '', true );
}
add_action( 'wp_enqueue_scripts', 'wpt_theme_js' );
// ----------------------------------------------------------------------------------------

// custom templates for specific categories
function get_custom_cat_template($single_template) {
     global $post;

       if ( in_category( 'credits' )) {
          $single_template = dirname( __FILE__ ) . '/single-credits.php';
       }
     return $single_template;
}

add_filter( "single_template", "get_custom_cat_template" ) ;
// ----------------------------------------------------------------------------------------

// Disable new content button
function disable_new_content() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('new-content');
}
add_action( 'wp_before_admin_bar_render', 'disable_new_content' );
// ----------------------------------------------------------------------------------------

// Remove WordPress icon from admin menubar
add_action( 'admin_bar_menu', 'remove_wp_logo', 999 );
function remove_wp_logo( $wp_admin_bar ) {
	$wp_admin_bar->remove_node( 'wp-logo' );
}
// ----------------------------------------------------------------------------------------

// Pagintation Title Fix
add_filter('navigation_markup_template', 'my_navigation_template', 10, 2 );
function my_navigation_template( $template, $class ){
	return '<nav class="navigation %1$s" role="navigation"><div class="nav-links">%3$s</div></nav>';
}
// ----------------------------------------------------------------------------------------

// limiting exchange user
function login_redirect( $redirect_to, $request, $user ){
	return admin_url('admin.php?page=exchange-rates');
}
add_filter('login_redirect','login_redirect',10,3);

add_action('admin_init', 'remove_profile_menu');
function remove_profile_menu() {
	global $wp_roles;
	remove_submenu_page('users.php', 'profile.php');
	$wp_roles->remove_cap('editor', 'read');
}

// ACF options page
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title' 	=> 'Հիմնական',
        'menu_title'	=> 'Հիմնական',
        'menu_slug' 	=> 'general-settings',
        'capability'	=> 'edit_posts',
        'redirect'		=> false
    ));
    acf_add_options_page(array(
        'page_title' 	=> 'Արտարժույթի փոխարժեքներ',
        'menu_title'	=> 'Արտ. փոխարժեքներ',
        'menu_slug' 	=> 'exchange-rates',
        'capability'	=> 'edit_posts',
        'redirect'		=> false
    ));
    acf_add_options_page(array(
        'page_title' 	=> 'Ոսկու գ.-մի արժեքներ',
        'menu_title'	=> 'Ոսկու գ.-մի արժեքներ',
        'menu_slug' 	=> 'gold-rates',
        'capability'	=> 'edit_posts',
        'redirect'		=> false
    ));
}
