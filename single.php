<?php get_header(); ?>

<?php if( 'en_US' == get_locale() ): ?>
  <?php $other_news_title = 'Other news'; ?>
<?php endif; ?>

<?php if( 'ru_RU' == get_locale() ): ?>
  <?php $other_news_title = 'Другие новости'; ?>
<?php endif; ?>

<?php if( 'hy' == get_locale() ): ?>
  <?php $other_news_title = 'Այլ նորություններ'; ?>
<?php endif; ?>

<?php if (have_posts()) : ?>
  <?php while (have_posts()) : the_post(); ?>
    <main class="d-flex flex-column bg-faded pb-4">
      <div class="page-header news-single-header">
        <div class="d-flex flex-column align-items-center justify-content-center pt-5 pb-4">
          <h2 class="title d-flex align-items-center justify-content-center text-center text-white w-100 mb-0 px-3"><?php the_title() ?></h2>

          <div class="credit-information w-100 mt-0 small d-flex align-items-center justify-content-center">
            <h6 class="mb-0 p-1 rounded font-weight-normal"><?php echo get_the_date(); ?></h6>
          </div>
        </div>
      </div>

      <div class="container mt-4">
        <div class="row">
          <div class="col-md-8 text-justify">
            <?php the_field('homepage_latest_news_content') ?>
          </div>

          <div class="col-md-4">
            <div class="row">
              <div class="col-12">
                <?php $image = get_field('homepage_latest_news_credit_cover_image') ?>
                <img class="img-fluid rounded" src="<?php echo $image['url']; ?>" alt="<?php echo $imageAlt ?>">
              </div>

              <!-- other images -->
              <?php $images = get_field('homepage_latest_news_other_images');
              if ($images): ?>
                <div class="d-flex flex-sm-wrap mt-3">
                  <?php foreach ($images as $image): ?>
                    <div class="col-sm-6 mb-3">
                      <img class="img-fluid rounded" src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $imageAlt ?>"/>
                    </div>
                  <?php endforeach; ?>
                </div>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>

      <div class="container mt-auto">
        <hr class="my-4">

        <h4 class="text-center mb-2"><?php echo $other_news_title ?></h4>

        <div class="row latest-news latest-news-single">
			    <?php $homepage_latest_news_query = new WP_Query( array( 'posts_per_page' => 3, 'post__not_in' => array($currentID), 'cat' => 4 ) ); while($homepage_latest_news_query->have_posts()) : $homepage_latest_news_query->the_post(); ?>

				    <?php $image = get_field('homepage_latest_news_credit_cover_image'); if( !empty($image) ):
					    $size = 'medium';
					    $thumb = $image['sizes'][ $size ];
					    ?>
				    <?php endif; ?>

            <div class="col-md-4 my-2">
              <div class="card h-100 d-flex flex-row">
                <a class="col-5 px-0" href="<?php the_permalink() ?>">
                  <img class="card-img-top" src="<?php echo $thumb; ?>" alt="<?php echo $imageAlt ?>">
                </a>

                <div class="col-7 card-body d-flex flex-column">
                  <a class="text-dark" href="<?php the_permalink() ?>"><h6 class="mb-0"><?php the_title() ?></h6></a>

                  <p class="card-text small"><small class="text-muted"><?php echo get_the_date( ); ?></small></p>

                  <a href="<?php the_permalink() ?>" class="btn btn-sm btn-warning mr-auto mt-auto">Կարդալ ավելին</a>
                </div>
              </div>
            </div>

			    <?php endwhile; ?>
			    <?php wp_reset_postdata(); ?>
        </div>
      </div>
    </main>
  <?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
