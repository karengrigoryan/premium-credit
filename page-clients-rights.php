<?php /* Template Name: Clients rights */ ?>
<?php get_header(); ?>

<main class="page-content">
	<div class="page-header bg-dark">
		<div class="d-flex flex-column align-items-center justify-content-center py-7">
			<h2 class="title text-center text-white mb-0 px-3"><?php the_title() ?></h2>
		</div>
	</div>

	<div class="container mt-5">
		<embed class="w-100" height="1200" src="<?php echo get_template_directory_uri(); ?>/src/docs/rights.pdf" alt="pdf">
	</div>
</main>

<?php get_footer(); ?>
