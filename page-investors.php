<?php /* Template Name: Investors */ ?><?php get_header(); ?>

<?php if ( 'en_US' == get_locale() ): ?>
	<?php $percents_title = 'Participation share:' ?><?php $shares_title = 'Number of shares:' ?>
<?php endif; ?>

<?php if ( 'ru_RU' == get_locale() ): ?>
	<?php $percents_title = 'Доля:' ?><?php $shares_title = 'Число акций:' ?>
<?php endif; ?>

<?php if ( 'hy' == get_locale() ): ?>
	<?php $percents_title = 'Մասնաբաժնի չափ`' ?><?php $shares_title = 'Բաժնետոմսերի քանակ`' ?>
<?php endif; ?>

<main id="page-investors" class="bg-faded pb-3">
  <div class="page-header bg-dark">
    <div class="d-flex flex-column align-items-center justify-content-center py-7">
      <h2 class="title text-center text-white mb-0 px-3"><?php the_title() ?></h2>
    </div>
  </div>

  <div class="container">
    <div class="my-5">
			<?php the_field( 'page_intro_text' ) ?>
    </div>

    <div class="row">
			<?php if ( have_rows( 'page_investors_members' ) ): ?><?php while ( have_rows( 'page_investors_members' ) ) : the_row(); ?>

				<?php $image = get_sub_field( 'photo' );
				if ( ! empty( $image ) ): $size = 'medium';
					$thumb                        = $image['sizes'][ $size ]; ?>
				<?php endif; ?>


        <div class="card shadow flex-lg-row mb-5 mx-2 py-4 px-2">
					<?php if ( $image ): ?>
            <div class="col-12 col-lg-3 text-center mb-2 mb-lg-0">
              <img class="img-fluid rounded-circle shadow-xs user-avatar" src="<?php echo $thumb; ?>" alt="<?php echo $imageAlt ?>">
            </div>
					<?php endif; ?>

          <div class="col-12 col-lg">
            <h4 class="text-center text-lg-left mb-1">
							<?php the_sub_field( 'name' ) ?>
            </h4>

            <div class="d-flex flex-wrap flex-md-nowrap align-items-center justify-content-center justify-content-lg-start mb-3 mb-lg-0">
              <div class="d-flex align-items-center">
                <p class="mr-1 mb-0"><?php echo $percents_title ?></p>

                <p class="lead mr-2 mb-0"><span class="badge badge-primary"><?php the_sub_field( 'percent' ) ?>%</span>
                </p>
              </div>

              <div class="d-flex align-items-center">
                <p class="mr-1 mb-0"><?php echo $shares_title ?></p>

                <p class="lead mb-0"><span class="badge badge-danger"><?php the_sub_field( 'share' ) ?></span></p>
              </div>
            </div>

            <div class="small text-muted mt-3">
							<?php the_sub_field( 'details' ) ?>
            </div>
          </div>
        </div>
			<?php endwhile; ?><?php endif; ?>
    </div>
  </div>
</main><?php get_footer(); ?>
