<?php get_header(); ?>

<?php if ( 'en_US' == get_locale() ): ?>
	<?php
	$errorPageTitle      = 'Page not found';
	$errorPageBackButton = 'Go to homepage';
	?>
<?php endif; ?>
<?php if ( 'ru_RU' == get_locale() ): ?>
	<?php
	$errorPageTitle      = 'Страница не найдена';
	$errorPageBackButton = 'Вернуться на главную страницу';
	?>
<?php endif; ?>
<?php if ( 'hy' == get_locale() ): ?>
	<?php
	$errorPageTitle      = 'Էջը չի գտնվել';
	$errorPageBackButton = 'Վերադառնալ գլխավոր էջ';
	?>
<?php endif; ?>

<main class="d-flex flex-column bg-faded">
  <div class="page-header bg-danger">
    <div class="d-flex flex-column align-items-center justify-content-center py-7">
      <h2 class="title text-center text-white mb-0 px-3"><?php echo $errorPageTitle ?></h2>
    </div>
  </div>

  <div class="container mw-100">
    <div class="row">
      <div class="col-12 text-center my-10">
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="btn btn-sm btn-primary hidden-sm-up">
          <i class="fa fa-home mr-1" aria-hidden="true">
          </i><?php echo $errorPageBackButton ?>
        </a>
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="btn btn-lg btn-primary hidden-xs-down">
          <i class="fa fa-lg fa-home mr-1" aria-hidden="true">
          </i><?php echo $errorPageBackButton ?>
        </a>
      </div>
    </div>
  </div>

  <div class="credits-carousel owl-carousel">
		<?php $currentID        = get_the_ID();
		$homepage_credits_query = new WP_Query( array(
				'posts_per_page' => 10,
				'post__not_in'   => array( $currentID ),
				'cat'            => 3
		) );
		while ( $homepage_credits_query->have_posts() ) : $homepage_credits_query->the_post(); ?>

			<?php $image = get_field( 'credit_cover_image' );
			if ( ! empty( $image ) ):
				$size  = 'medium';
				$thumb = $image['sizes'][ $size ];
				?>
			<?php endif; ?>

      <a href="<?php the_permalink() ?>">
        <div class="item">
          <img class="w-100 h-100" src="<?php echo $thumb; ?>" alt="<?php echo $imageAlt ?>">
          <div class="overlay"></div>
          <p class="title d-flex align-items-center justify-content-center text-center w-100 h-100 mb-0 px-3"><?php the_title() ?></p>
        </div>
      </a>

		<?php endwhile; ?>
		<?php wp_reset_postdata(); ?>
  </div>
</main>

<?php get_footer(); ?>
