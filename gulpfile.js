var gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync').create(),
    sourcemaps = require('gulp-sourcemaps'),
    image = require('gulp-image'),
    cleanCSS = require('gulp-clean-css'),
    gutil = require('gulp-util'),
    critical = require('critical').stream;

gulp.task('image', function () {
    gulp.src('src/img/**/*')
        .pipe(image())
        .pipe(gulp.dest('src/img/'));
});

gulp.task('sass', function () {
    return gulp.src('scss/style.scss')
        .pipe(sass())
        .pipe(gulp.dest('./'))
        .pipe(browserSync.reload({
            stream: true
        }))
});

gulp.task('browserSync', function () {
    var files = [
        './src/**/*',
        './*.php'
    ];

    browserSync.init(files, {
        notify: false,
        proxy: "localhost:8888/premium-credit/",
        open: true,
        browser: "google chrome canary"
    })
})

gulp.task('minify-css', function () {
    return gulp.src('./style.css')
        .pipe(sourcemaps.init())
        .pipe(cleanCSS({compatibility: '*'}))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./'));
});

gulp.task


gulp.task('critical', function () {
    return gulp.src('./*.php')
        .pipe(critical({
            base: './',
            inline: true,
            css: './style.css',
            dimensions: [{
                height: 568,
                width: 320
            }, {
                height: 800,
                width: 1280
            }]
        }))
        .on('error', function(err) { gutil.log(gutil.colors.red(err.message)); })
        .pipe(gulp.dest('dist'));
});

gulp.task('watch', ['browserSync', 'sass'], function () {
    gulp.watch('scss/**/*.scss', ['sass']);
});

gulp.task('watch-no', ['sass'], function () {
    gulp.watch('dev/scss/**/*.scss', ['sass']);
});
