<?php /* Template Name: Contacts */ ?>
<?php get_header(); ?>

<?php if ( 'en_US' == get_locale() ): ?>
	<?php
	$contact_form = do_shortcode( '[contact-form-7 id="195" title="Contact Us"]' );
	$address      = get_field( 'main_en_address', 'options' );
	?>
<?php endif; ?>

<?php if ( 'ru_RU' == get_locale() ): ?>
	<?php
	$contact_form = do_shortcode( '[contact-form-7 id="215" title="Обратная связь"]' );
	$address      = get_field( 'main_ru_address', 'options' );
	?>
<?php endif; ?>

<?php if ( 'hy' == get_locale() ): ?>
	<?php
	$contact_form = do_shortcode( '[contact-form-7 id="6" title="Հետադարձ կապ"]' );
	$address      = get_field( 'main_address', 'options' );
	?>
<?php endif; ?>

<main id="contacts" class="bg-faded">
  <div id="google-maps" class="w-100"></div>
  <script type="text/javascript">
      function initMap() {
          var mapOptions = {
              zoom: 17,
              scrollwheel: false,
              center: new google.maps.LatLng(40.175495, 44.517903),
              disableDefaultUI: true,

              styles: [
                  {
                      "featureType": "administrative.country",
                      "elementType": "geometry",
                      "stylers": [
                          {
                              "visibility": "simplified"
                          },
                          {
                              "hue": "#ff0000"
                          }
                      ]
                  }
              ]
          };

          var mapElement = document.getElementById('google-maps');
          var map = new google.maps.Map(mapElement, mapOptions);

          var marker = new google.maps.Marker({
              map: map,
              position: new google.maps.LatLng(40.175295, 44.517903),
              icon: "<?php echo get_template_directory_uri(); ?>/src/img/pin.svg"
          })
      }
  </script>

  <div class="container">
    <div class="row my-5">
      <div class="col-lg-8">
        <h2><?php the_field( 'contacts_page_title' ) ?></h2>

        <p class="mb-5"><?php the_field( 'contacts_required_field_text' ) ?></p>

				<?php echo $contact_form ?>
      </div>

      <div class="col-lg-4 pl-lg-5 mt-5 mt-lg-0">
        <div class="text-center text-white call-us-section rounded shadow-xs mb-3 p-3">
          <h5 class="mt-0"><?php the_field( 'contacts_tel_title' ) ?></h5>

          <ul class="list-unstyled mb-0 pl-0">
            <li class="h4 font-weight-normal">(011) 290000</li>
            <li class="h4 font-weight-normal">(011) 293000</li>
            <li class="h4 font-weight-normal">(011) 294000</li>
          </ul>
        </div>

        <div class="p-3 text-center text-white address-section rounded shadow-xs">
          <h5 class="mt-0"><?php the_field( 'contacts_address_title' ) ?></h5>

          <h5 class="font-weight-normal mb-0 ml-auto"><?php echo $address ?></h5>
        </div>

        <div class="py-3">
          <hr>

          <h5 class="my-3"><?php the_field( 'contacts_open_hours_title' ) ?></h5>

					<?php the_field( 'contacts_open_hours_content' ) ?>
        </div>
      </div>
    </div>
  </div>
</main>

<?php get_footer(); ?>
