<?php get_header(); ?>
<main class="bg-faded">
	<div class="page-header bg-dark">
		<div class="d-flex flex-column align-items-center justify-content-center py-7">
			<h2 class="title text-center text-white mb-0 px-3"><?php the_title() ?></h2>
		</div>
	</div>

	<div class="container">
		<div class="my-5">
		  <?php the_field('page_content') ?>
    </div>
	</div>

</main>
<?php get_footer(); ?>
